function prod_dest(c)
#Production destruction matrices for a Robertson model
d= length(c)
p=zeros(d,d)
d=zeros(d,d)
p[1,2]=10^4*c[2]*c[3]
d[2,1]=10^4*c[2]*c[3]
p[2,1]=0.04*c[1]
d[1,2]=0.04*c[1]
p[3,2]=3*10^7*c[2]^2
d[2,3]=3*10^7*c[2]^2
return p, d
end

