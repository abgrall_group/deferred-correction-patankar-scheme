using SymPy, FastGaussQuadrature


function generate_poly(s, nodes,deg)
#Generates Lagrangian polynomial in symbolic

aux=[]

for k=1:deg+1
    append!(aux, 1);
    for j=1:deg+1
        if j != k
            aux[k]=aux[k].*(s-nodes[j]);
        end
    end
end
for j=1:deg+1
    aux[j]= aux[j]/SymPy.N(subs(aux[j],s,nodes[j]));
end
return aux
end

function compute_theta(M_sub, nodes_type)
# Compute the integral of lagrange polynomials from 0 to interpolation point theta[r,m]= \int_{t^0}^{t^m} \phi_r
s=Sym("s");
if nodes_type =="equispaced"
    nodes=range(0,stop=1,length=M_sub+1);
elseif nodes_type=="gaussLobatto"
    nodes, w = gausslobatto(M_sub+1)
    nodes = nodes ./2.0 .+0.5
end

poly=generate_poly(s,nodes,M_sub)

theta=zeros(M_sub+1, M_sub+1)

for r=1:M_sub+1
    for m=1:M_sub+1
        theta[r,m]=integrate(poly[r],(s,0,nodes[m]))
    end
end

return theta

end
