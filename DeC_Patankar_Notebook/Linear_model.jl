function prod_dest(c)
#Production destruction matrices for a linear model
a=parameters()
d= length(c)
p=zeros(d,d)
d=zeros(d,d)
p[1,2]=c[2]
d[2,1]=c[2]
p[2,1]=a*c[1]
d[1,2]=a*c[1]
return p, d
end

"""Exact Solution for the linear Problem """
function exact_solution(c0, time)
a= parameters()
c1_inf= sum(c0)/(a+1)
c = c0[1]/c1_inf -1.0

c1 = (1+c*exp(-(a+1)*time))*c1_inf
c2=sum(c0)-c1
return [c1,c2]

end


function parameters()
a=5.0
return a
end
