using Plots
using LinearAlgebra

include("Lagrange_polynomials.jl")
include("DeC_procedure.jl")

#3 possible tests
test="robertson" #     "non_linear"#  "linear"  #

#number of timesteps
N_time = 100
# order d => subtimesteps M=order-1, corrections = order
nodes_type  = "gaussLobatto"  # "equispaced"
order=4
K_iter=order

# number of nodes depends on the type of nodes (gauss Lobatto with M+1 nodes has accuracy 2M)
if nodes_type=="equispaced"
  M_sub = order-1
elseif nodes_type=="gaussLobatto"
  M_sub = ceil(Integer,order/2)
end

save_theta_matrices(order, nodes_type)

# Test data: initial conditions, times
if test=="linear"
  include("Linear_model.jl")
  u0 = [0.9;0.1]
  T_0=0
  T_final = 1.75
  ts=range(T_0,stop=T_final,length=N_time+1)
elseif test=="robertson"
  epsilon= Base.eps()
  u0=[1.0-2.0*epsilon;epsilon;epsilon]

  T_0 = 10^-6
  T_final = 10^10

  "Variable timesteps in logaritmic scale"
  logdt0 = (log(T_final)-log(T_0))/N_time
  dt0=exp(logdt0)
  ts= zeros(N_time+1)
  ts[1]=T_0
  for it=1:N_time
    ts[it+1]=T_0*dt0^it
  end

  include("Robertson_model.jl")
elseif test=="non_linear"
  u0 = [9.98;0.01;0.01]
  T_0=0
  T_final = 30.
  ts=range(T_0,stop=T_final,length=N_time+1)
  include("Nonlinear_model.jl")
else
  println("Not valid IC")
end

#Computing the dec patankar method 
@time dts, U = dec_correction( prod_dest,  M_sub, K_iter, u0, ts ) 
@time dts, U = dec_correction( prod_dest,  M_sub, K_iter, u0, ts ) 

#Plotting the solutions
if test=="robertson"
#For robertson test case, we multiply the second variable times 10^4 to be visible
  U[2,:]=10^4*U[2,:]
  plot(ts,transpose(U), xaxis=:log)
else
  plot(ts,transpose(U))
end  
plot!(title="Test $(test)",
     xaxis="Time",yaxis="Values", legend=:best)
savefig("../Figures/$(test)_N$(N_time).pdf")
