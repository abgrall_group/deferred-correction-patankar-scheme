using LinearAlgebra
using NPZ

include("Lagrange_polynomials.jl")

max_order = 10

matrices_folder = "theta_DeC_matrices"

function save_theta_matrices(max_order, nodes_type)
  # Create matrix folder
  mkpath(matrices_folder)
  println("Computing with symbolic tools the theta coefficients of the DeC")
  for M_sub = 1:max_order
    matrix_file = joinpath(matrices_folder,"theta_$(nodes_type)_$(M_sub).npz")
    if ~isfile(matrix_file)
      Theta=compute_theta(M_sub, nodes_type)
      println("M_sub $(M_sub)")
      npzwrite(matrix_file, Theta)
    end
  end
end


function read_theta_matrix(M_sub)
  matrix_file = joinpath(matrices_folder,"theta_$(nodes_type)_$(M_sub).npz")
  Theta = npzread(matrix_file)
  return Theta
end


function dec_correction( prod_dest,  M_sub::Int, K_corr::Int, y_0, ts)


    #Initial data, time(0), timesteps, dimension of the system
    t_0 = ts[1] 
    N_time = length(ts)-1
    dim=length(y_0)
    #Variables, U is the solution for all timesteps
    U=zeros(dim, N_time+1)
    #Variables of the DeC procedure: one for each subtimesteps and one for previous correction up, one for current correction ua
    u_p=zeros(dim, M_sub+1)
    u_a=zeros(dim, M_sub+1)

    #Matrices of production and destructions applied to up at different subtimesteps
    prod_p=zeros(dim,dim,M_sub+1)
    dest_p=zeros(dim,dim,M_sub+1)

    #coefficients for time integration
    Theta=read_theta_matrix(M_sub)

    U[:,1]=y_0
    delta_t=zeros(N_time)

    #Timesteps loop
    for it=2: N_time+1
        #computing actual delta t
        delta_t[it-1]=ts[it]-ts[it-1]

        # Subtimesteps loop to update variables at iteration 0
        for m=1:M_sub+1
         u_a[:,m]=U[:,it-1]
         u_p[:,m]=U[:,it-1]
        end
        #Loop for iterations K
        for k=2:K_corr+1
            # Updating previous iteration variabls
            u_p=copy(u_a)
            #Loop on subtimesteps to compute the production destruction terms
            for r=1:M_sub+1
                prod_p[:,:,r], dest_p[:,:,r]=prod_dest(u_p[:,r])
            end
            # Loop on subtimesteps to compute the new variables
            for m=2:M_sub+1
                u_a[:,m]=patanker_type_dec(prod_p, dest_p, delta_t[it-1], m, M_sub, Theta, u_p, dim)
            end
        end
        # Updating variable solution
        U[:,it]=u_a[:,M_sub+1]
    end


    return delta_t, U
end


"""prod_p and dest_p dim x dim
delta_t is timestep length
m_substep is the subtimesteps for which we are solving the system
M_sub is the maximum number of subtimesteps (0,..., M)
theta coefficient vector matrix  in  M_sub+1 x M_sub+1, first index is the index we are looping on,
the second is the one we are solving for, theta_r,m= int_t0^tm phi_r
u_p is the previous correction solution for all subtimesteps in dim x (M_sub+1) """
function patanker_type_dec(prod_p, dest_p, delta_t,  m_substep::Int, M_sub, theta, u_p, dim)
#This function builds and solves the system u^{(k+1)}=Mu^{(k)} for a subtimestep m

mass=Matrix{Float64}(I, dim, dim);
#println(mass)
for i=1:dim
    for r=1: M_sub+1
        if theta[r,m_substep]>0
            for j= 1: dim
              mass[i,j]=mass[i,j]- delta_t*theta[r,m_substep]*(prod_p[i,j,r]/u_p[j,m_substep])
              mass[i,i]=mass[i,i]+ delta_t*theta[r,m_substep]*(dest_p[i,j,r]/u_p[i,m_substep])
            end
        elseif theta[r,m_substep]<0
            for j= 1: dim
              mass[i,i]=mass[i,i]- delta_t*theta[r,m_substep]*(prod_p[i,j,r]/u_p[i,m_substep])
              mass[i,j]=mass[i,j]+ delta_t*theta[r,m_substep]*(dest_p[i,j,r]/u_p[j,m_substep])
            end
        end
    end
end
return mass\u_p[:,1]
end

