function prod_dest(c)
#Production destruction matrices for a nonlinear model
a=parameters_non()
d= length(c)
p=zeros(d,d)
d=zeros(d,d)
d[1,2]=c[1]*c[2]/(c[1]+1.)
p[2,1]=c[1]*c[2]/(c[1]+1.)
p[3,2]=a*c[2]
d[2,3]=a*c[2]
return p, d
end

function parameters_non()
a=0.3
return a
end
