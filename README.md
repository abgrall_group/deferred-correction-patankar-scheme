# Deferred Correction Patankar scheme (**mPDeC**)

In this repository we provide a small code written in Julia to test the high-order, positivity preserving, conservative DeC Patankar scheme. Details can be found here https://arxiv.org/abs/1905.09237

## Installation
This code was tested on Julia-1.10.0 and requires the packages LinearAlgebra, Plots, NPZ, FastGaussQuadrature and SymPy. To download Julia just visit [Julia webpage](https://julialang.org/) 

## Usage
In notebook folder one can find the library files. The files "*_model.jl" contain the production destruction function for the specific model. These are used by the "DeC_procedure.jl" function, that is the core of the algorithm and can be used by
```julia
dts, U = dec_correction( prod_dest,  M_sub, K_iter, u0, ts ) # model function, subtimesteps, iterations of DeC, initial conditions, times
```

In "Time_integration.jl" one can run some prepared testcases, just changing the test name in 
```julia
test="robertson" #"linear"  #"non_linear" #choose a model
```
You can also change order of the scheme e type of nodes for the DeC (between equispaced and Gauss--Lobatto).
```julia
nodes_type  = "gaussLobatto"  # "equispaced"
order=4
```


A figure will be produced in the folder Figures.

## Authors
**mPDeC** has been developed at University of Zurich by Philipp &Ouml;ffner and Davide Torlo.
